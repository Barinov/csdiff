﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.Common;
using System.Net;
using Microsoft.TeamFoundation.Framework.Client;
using System.Collections.ObjectModel;
using Microsoft.TeamFoundation.Framework.Common;
using Microsoft.TeamFoundation.VersionControl.Client;
using Microsoft.TeamFoundation.VersionControl.Common;
using System.IO;
using System.Windows.Forms;
using Microsoft.TeamFoundation.Server;
using Microsoft.TeamFoundation;

namespace CSDiff
{
    class Program
    {
        static private TfsTeamProjectCollection projectCollection = null;
        static private VersionControlServer versionControl = null;

        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                var name = System.AppDomain.CurrentDomain.FriendlyName;

                Console.Error.WriteLine("{0} — saves diff file for the specified changeset.", name);
                Console.Error.WriteLine("");
                Console.Error.WriteLine("Usage:");
                Console.Error.WriteLine("   {0} <changeset1> [<changeset2> [...]]", name);
                Console.Error.WriteLine("");
                Console.Error.WriteLine("Example:");
                Console.Error.WriteLine("   {0} 1941909 — saves changeset #1941909", name);
                return;
            }

            try
            {
                if (string.IsNullOrEmpty(Properties.Settings.Default.TfsUri))
                {
                    throw new Exception();
                }

                Console.Out.WriteLine("Connecting to TFS server at {0}...", Properties.Settings.Default.TfsUri);
                projectCollection = new TfsTeamProjectCollection(new Uri(Properties.Settings.Default.TfsUri));
                versionControl = projectCollection.GetService<VersionControlServer>();
            }
            catch (Exception)
            {
                Console.Error.WriteLine("Could not connect to TFS. Check that you have right TFS connection string in configuration file.");
                return;
            }

            foreach (var arg in args)
            {
                var changeSetId = 0;
                try
                {
                    changeSetId = int.Parse(arg);
                }
                catch (FormatException)
                {
                    continue;
                }

                Console.Out.Write("Processing changeset {0}...", changeSetId);
                StreamWriter file = new System.IO.StreamWriter(String.Format("{0}.diff", changeSetId));
                file.AutoFlush = true;

                var changeSet = versionControl.GetChangeset(changeSetId);

                file.WriteLine("# Changeset: {0}", changeSetId);
                file.WriteLine("# User: {0}", changeSet.Owner);
                file.WriteLine("# Checked in by: {0}", changeSet.Committer);
                file.WriteLine("# Date: {0}", changeSet.CreationDate);
                file.WriteLine("#");

                /* Comment */
                if (changeSet != null && changeSet.Comment != null && changeSet.Comment.Length > 0)
                {
                    file.WriteLine("# Comment:");
                    foreach (var line in changeSet.Comment.TrimEnd().Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None))
                    {
                        file.WriteLine("#   {0}", line);
                    }
                    file.WriteLine("#");
                }

                /* Changed files */
                if (changeSet != null && changeSet.Changes != null && changeSet.Changes.Length > 0)
                {
                    file.WriteLine("# Items:");
                    foreach (var change in changeSet.Changes)
                    {
                        file.WriteLine("#   {0} {1}", change.ChangeType, change.Item.ServerItem);
                    }
                    file.WriteLine("#");
                }

                /* Associated work items */
                if (changeSet != null && changeSet.AssociatedWorkItems != null && changeSet.AssociatedWorkItems.Length > 0)
                {
                    file.WriteLine("# Work Items:");

                    int idLength = "ID".Length;
                    int typeLength = "Type".Length;
                    int titleLength = "Title".Length;

                    foreach (var workItem in changeSet.AssociatedWorkItems)
                    {
                        idLength = Math.Max(idLength, workItem.Id.ToString().Length);
                        typeLength = Math.Max(typeLength, workItem.WorkItemType.Length);
                        titleLength = Math.Max(titleLength, workItem.Title.Length);
                    }

                    var formatString = String.Format("#   {{0,-{0}}} {{1,-{1}}} {{2,-{2}}}", idLength, typeLength, titleLength);
                    file.WriteLine(formatString, "ID", "Type", "Title");
                    file.WriteLine(formatString, new String('-', idLength), new String('-', typeLength), new String('-', titleLength));

                    foreach (var workItem in changeSet.AssociatedWorkItems)
                    {
                        file.WriteLine(formatString, workItem.Id.ToString(), workItem.WorkItemType, workItem.Title);
                    }
                    file.WriteLine("#");
                }

                /* Check in notes */
                if (changeSet != null && changeSet.CheckinNote != null && changeSet.CheckinNote.Values.Length > 0)
                {
                    file.WriteLine("# Check-in Notes:");
                    foreach (var note in changeSet.CheckinNote.Values)
                    {
                        file.WriteLine("#   {0}: {1}", note.Name, note.Value);
                    }
                }

                /* Diff */
                if (changeSet != null && changeSet.Changes != null && changeSet.Changes.Length > 0)
                {
                    DiffOptions diffOptions = new DiffOptions();
                    diffOptions.UseThirdPartyTool = false;
                    diffOptions.OutputType = DiffOutputType.Unified;
                    diffOptions.StreamWriter = file;

                    foreach (var change in changeSet.Changes)
                    {
                        if (change.ChangeType.HasFlag(Microsoft.TeamFoundation.VersionControl.Client.ChangeType.Add))
                        {
                            var path = change.Item.ServerItem;

                            var sourceVersion = VersionSpec.ParseSingleSpec(String.Format("C{0}", changeSetId), versionControl.AuthorizedUser);
                            versionControl.DownloadFile(path, 0, sourceVersion, "csdiff.tmp");


                            file.WriteLine(path);
                            file.WriteLine("===================================================================");
                            file.WriteLine("--- /dev/null");
                            file.WriteLine("+++ Server: {0};C{1}", path, changeSetId);
                            file.WriteLine("@@ -0,0 +1,{0} @@", File.ReadLines("csdiff.tmp").Count());

                            foreach (var line in File.ReadLines("csdiff.tmp"))
                            {
                                file.WriteLine("+{0}", line);
                            }

                            file.WriteLine("===================================================================");
                            File.Delete("csdiff.tmp");
                        }
                        else
                        {
                            var path = change.Item.ServerItem;

                            var sourceVersion = VersionSpec.ParseSingleSpec(String.Format("C{0}", changeSetId - 1), versionControl.AuthorizedUser);
                            var targetVersion = VersionSpec.ParseSingleSpec(String.Format("C{0}", changeSetId), versionControl.AuthorizedUser);

                            var sourceFile = new DiffItemVersionedFile(versionControl, path, sourceVersion);
                            var targetFile = new DiffItemVersionedFile(versionControl, path, targetVersion);

                            Difference.DiffFiles(versionControl, sourceFile, targetFile, diffOptions, path, true);
                        }
                    }
                }

                file.Close();
                Console.Out.WriteLine(" done");

            }
        }
    }
}
